defmodule BTCPay.MixProject do
  use Mix.Project

  def project do
    [
      app: :btcpay,
      version: "0.1.0",
      elixir: "~> 1.11",
      description: "Client library for BTCPay Server.",
      source_url: "https://gitlab.com/tribes-host/btcpay_elixir",
      package: package(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/tribes-host/btcpay_elixir"}
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:httpoison, "~> 1.6"},
      {:jason, "~> 1.0"},
      {:plug, "~> 1.11"}
    ]
  end
end
