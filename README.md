# BTCPay Elixir

Client library for [BTCPay Server](https://btcpayserver.org/).

This library implements the [Greenfield API](https://docs.btcpayserver.org/API/Greenfield/v1/), allowing users to manage resources across multiple stores.
So far only a subset of the API has been implemented.
It can handle the basic use case of generating an invoice and then handling webhook events.

- [x] Invoices
- [x] Payment Requests
- [ ] Webhooks
- [x] Webhook Events
- [ ] Stores
- [ ] Authorization
- [ ] API Keys
- [ ] Users
- [ ] User Notifications
- [ ] Lightning
- [ ] Pull Payments

## Installation

The package can be installed by adding `btcpay` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:btcpay, "~> 0.1.0"}
  ]
end
```

## Configuration

You will need to generate an API key in BTCPay Server for your user under `My settings > API Keys`.
Then configure your host and API key in your application's environment.

```elixir
config :btcpay,
  host: "btcpay.example.com",
  api_key: "42f04397b476d38ec498b70e212b55cdfbfcc766"
```

## Usage

```elixir
# Example payment action
def pay(conn, _params) do
  store = "8d4Uhm8Tb898LavnfKyUAApKJ2vWe1AW7VqnaQoz73ed"

  # Initialize an Invoice struct
  invoice = %BTCPay.Invoice{
    amount: 10,
    currency: "usd",
    metadata: %{
      my_id: "1234"
    },
    checkout: %{
      redirectURL: "https://mysite.com/"
    }
  }

  # POST the Invoice to BTCPay backend
  with {:ok, %BTCPay.Invoice{} = invoice} <- BTCPay.Invoice.create(invoice, store) do

    # Redirect the user to the Invoice page
    url = BTCPay.Invoice.pay_url(invoice)
    redirect(conn, external: url)
  end
end
```

## Webhook Events

To handle webhook events, first you must configure the endpoint.
Add the following to `endpoint.ex`, **BEFORE** `Plug.Parsers` is loaded.

```elixir
plug BTCPay.WebhookPlug,
  at: "/webhook/btcpay", # whatever route you want
  handler: MyAppWeb.BTCPayEventHandler,
  secret: Application.get_env(:myapp, :btcpay_webhook_secret)
```

**Note:** If you're loading config dynamically at runtime (eg with `runtime.exs` or an OTP app) you must pass a tuple or function as the secret:

```elixir
plug BTCPay.WebhookPlug,
  at: "/webhook/btcpay",
  handler: MyAppWeb.BTCPayEventHandler,
  secret: {Application, :get_env, [:myapp, :btcpay_webhook_secret]}
```

You will need to create the `MyAppWeb.BTCPayEventHandler` to handle the events, which should look like this:

```elixir
defmodule MyAppWeb.BTCPayEventHandler do
  def handle_event(%{"type" => "InvoiceReceivedPayment"} = event) do
    # TODO: handle the InvoiceReceivedPayment event
  end

  # Return HTTP 200 for unhandled events
  def handle_event(_event), do: :ok
end
```

Finally, visit `Manage store > Webhooks` in BTCPay Server and add a webhook to your store, using the route you configured in the endpoint above.
You can then copy the webhook secret into your app's configuration.
