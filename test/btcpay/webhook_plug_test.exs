defmodule BTCPay.WebhookPlugTest do
  use ExUnit.Case
  use Plug.Test
  alias BTCPay.WebhookPlug

  @secret "mW5m3brwL4GGno541AbUimmXqBy"
  @opts WebhookPlug.init(
          at: "/webhook/btcpay",
          handler: __MODULE__.Handler,
          secret: @secret
        )

  defmodule Handler do
    def handle_event(%{"type" => "InvoiceProcessing"}), do: :ok
  end

  defmodule BadHandler do
    def handle_event(%{"type" => "InvoiceProcessing"}), do: nil
  end

  def get_value(:secret), do: @secret

  describe "WebhookPlug" do
    setup do
      body = """
      {
        "overPaid": false,
        "deliveryId": "YD4RAPETGNjnFSbrLZmMMP",
        "webhookId": "GveBmW1eWs3hmcg1emumpw",
        "orignalDeliveryId": "YD4RAPETGNjnFSbrLZmMMP",
        "isRedelivery": false,
        "type": "InvoiceProcessing",
        "timestamp": 1607451803,
        "storeId": "8d4Uhm8Tb898LavnfKyUAApKJ2vWe1AW7VqnaQoz73ed",
        "invoiceId": "EFJMXaxnMrPfY3wFBm6q6q"
      }\
      """

      valid_sig = "sha256=3960a905e7d44a04ab0f77a57a677ccc7d75f863625573b8f3f8f9c62e4e7c35"

      conn =
        conn(:post, "/webhook/btcpay", body)
        |> put_req_header("btcpay-sig", valid_sig)

      {:ok, %{conn: conn}}
    end

    test "rejects invalid signature", %{conn: conn} do
      invalid_sig = "sha256=572fefd3b646f2744240372b53500471ca673705436dd97117389869e91f74a3"
      conn = put_req_header(conn, "btcpay-sig", invalid_sig)

      result = WebhookPlug.call(conn, @opts)
      assert result.state == :sent
      assert result.status == 400
    end

    test "accepts valid signature", %{conn: conn} do
      result = WebhookPlug.call(conn, @opts)
      assert result.state == :sent
      assert result.status == 200
    end

    test "nil secret should raise an error", %{conn: conn} do
      opts = WebhookPlug.init(at: "/webhook/btcpay", handler: __MODULE__.Handler, secret: nil)

      assert_raise RuntimeError, fn ->
        WebhookPlug.call(conn, opts)
      end
    end

    test "function secret should be evaluated", %{conn: conn} do
      opts =
        WebhookPlug.init(
          at: "/webhook/btcpay",
          handler: __MODULE__.Handler,
          secret: fn -> @secret end
        )

      result = WebhookPlug.call(conn, opts)
      assert result.state == :sent
      assert result.status == 200
    end

    test "{m, f, a} secret should be evaluated", %{conn: conn} do
      opts =
        WebhookPlug.init(
          at: "/webhook/btcpay",
          handler: __MODULE__.Handler,
          secret: {__MODULE__, :get_value, [:secret]}
        )

      result = WebhookPlug.call(conn, opts)
      assert result.state == :sent
      assert result.status == 200
    end

    test "crash hard if handler fails", %{conn: conn} do
      opts =
        WebhookPlug.init(
          at: "/webhook/btcpay",
          handler: __MODULE__.BadHandler,
          secret: @secret
        )

      assert_raise RuntimeError, fn ->
        WebhookPlug.call(conn, opts)
      end
    end
  end
end
