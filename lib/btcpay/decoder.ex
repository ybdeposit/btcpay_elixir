defmodule BTCPay.Decoder do
  @moduledoc """
  Decodes BTCPay API responses into native Elixir structs.
  """

  def decode(struct, list) when is_list(list) do
    Enum.map(list, &decode(struct, &1))
  end

  def decode(struct, body) do
    attrs = Jason.decode!(body, keys: :atoms)
    struct(struct, attrs)
  end
end
