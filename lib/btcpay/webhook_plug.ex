defmodule BTCPay.WebhookPlug do
  import Plug.Conn
  alias Plug.Conn

  @behaviour Plug

  def init(opts) do
    path_info = String.split(opts[:at], "/", trim: true)

    opts
    |> Enum.into(%{})
    |> Map.put_new(:path_info, path_info)
  end

  def call(%Conn{method: "POST", path_info: path_info} = conn, %{
        path_info: path_info,
        secret: secret,
        handler: handler
      }) do
    secret = parse_secret!(secret)

    with [signature] <- get_req_header(conn, "btcpay-sig"),
         {:ok, body, _} = Conn.read_body(conn),
         true <- verify_signature?(body, secret, signature),
         {:ok, json} <- Jason.decode(body),
         :ok <- handle_event!(handler, json) do
      send_resp(conn, 200, "Webhook received.") |> halt()
    else
      _ -> send_resp(conn, 400, "Bad request.") |> halt()
    end
  end

  def call(%Conn{path_info: path_info} = conn, %{path_info: path_info}) do
    send_resp(conn, 400, "Bad request.") |> halt()
  end

  def call(conn, _), do: conn

  defp handle_event!(handler, json) do
    case handler.handle_event(json) do
      {:ok, _} ->
        :ok

      :ok ->
        :ok

      resp ->
        raise """
        #{inspect(handler)}.handle_event/1 returned an invalid response. Expected :ok or {:ok, _}
        Got: #{inspect(resp)}

        Event data: #{inspect(json)}
        """
    end
  end

  defp parse_secret!({m, f, a}), do: apply(m, f, a)
  defp parse_secret!(fun) when is_function(fun), do: fun.()
  defp parse_secret!(secret) when is_binary(secret), do: secret

  defp parse_secret!(secret) do
    raise """
    The BTCPay webhook secret is invalid. Expected a string, tuple, or function.
    Got: #{inspect(secret)}

    If you're setting the secret at runtime, you need to pass a tuple or function.
    For example:

    plug BTCPay.WebhookPlug,
      at: "/webhook/btcpay",
      handler: MyAppWeb.BTCPayEventHandler,
      secret: {Application, :get_env, [:myapp, :btcpay_webhook_secret]}
    """
  end

  defp verify_signature?(body, secret, signature)
       when is_binary(body) and is_binary(secret) and is_binary(signature) do
    signature ==
      :crypto.mac(:hmac, :sha256, secret, body)
      |> Base.encode16(case: :lower)
      |> (&"sha256=#{&1}").()
  end
end
